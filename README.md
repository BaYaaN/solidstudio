# LRU-CAHCE
1. Stack
- Spring Boot
- JDK 8
- Gradle
- Embedded Tomcat
- Swagger
- Rest assured
- Lombok

2. Build app
- gradlew clean build

3. Run App
-gradlew bootRun

3. Url to swagger
- http://localhost:8080/swagger

4. Solution
I tried to make as more as possible generic solution (in order to expose CacheService as ext lib). 
I choose LinkedHashMap as cache implement cause ensure insertion order and good performance for proper implemented key. 
Strategy pattern allowed us to use another cache implementation we just need to deliver implementation for new cache and run with proper profile.

5. Performance
Big O notation for LinkedHashMap (assumed that key are implement in proper way)
-put O(1)
-get O(1)
-remove amortized O(1)
I mainly focus on GET operation as it should be O(1) (that's why it return value immediately and invoke 'sorting' asynchronously)

If You have any question please email me :)

  





