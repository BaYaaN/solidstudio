package com.solidstudio.lru.cache.core.config;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootConfiguration
@ComponentScan("com.solidstudio.lru.cache.core")
public class CacheApplicationConfig {
}
