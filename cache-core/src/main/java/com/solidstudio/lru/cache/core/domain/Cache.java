package com.solidstudio.lru.cache.core.domain;

import java.util.concurrent.atomic.AtomicInteger;

public interface Cache <T> {
    AtomicInteger getSize();
    T getContent();
}
