package com.solidstudio.lru.cache.core.domain;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.Collections.synchronizedMap;

@Getter
@Profile("mapCache")
@Component
public final class MapCache<K, V> implements Cache<Map<K, V>> {
    private final AtomicInteger size;
    private final Map<K, V> content;

    public MapCache(final @Value("${cache.size}") Integer size) {
        this.size = new AtomicInteger(size);
        this.content = new LinkedHashMap<>(size);
    }
}
