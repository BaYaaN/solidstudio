package com.solidstudio.lru.cache.core.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PutModel<K, V> {
    @NotNull
    private K key;
    private V value;
}
