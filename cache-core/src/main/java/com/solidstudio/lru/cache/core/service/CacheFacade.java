package com.solidstudio.lru.cache.core.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public final class CacheFacade <K,V> {

    private final CacheService<K,V> cacheService;

    public V get(final K key) {
        return cacheService.get(key);
    }

    public void put(final K key, final V value) {
        cacheService.put(key, value);
    }

    public void changeCapacity(final int capacity) {
        cacheService.changeCapacity(capacity);
    }

    public void invalidate() {
        cacheService.invalidate();
    }
}
