package com.solidstudio.lru.cache.core.service;

public interface CacheService<K, V> {
    V get(final K key);

    void put(final K key, final V value);

    void changeCapacity(final Integer capacity);

    void invalidate();
}
