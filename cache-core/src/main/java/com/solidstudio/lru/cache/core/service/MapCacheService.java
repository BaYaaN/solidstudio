package com.solidstudio.lru.cache.core.service;

import com.solidstudio.lru.cache.core.domain.Cache;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

@Slf4j
@Service
@Profile("mapCache")
@AllArgsConstructor
public final class MapCacheService<K, V> implements CacheService<K, V> {

    private final Cache<Map<K, V>> cache;
    private final ExecutorService executor = Executors.newFixedThreadPool(10);

    @Override
    public V get(@NonNull final K key) throws NoSuchElementException {
        log.debug("Retrieving key: {} from cache", key);
        final Map<K, V> map = cache.getContent();

        if (!map.containsKey(key)) {
            log.warn("Key: {} not exist in cache", key);
            throw new NoSuchElementException("Key does not exist in cache");
        }

        final V value = map.get(key);

        moveAsynchronouslyToEndOfCache(map, key, value);

        return value;
    }

    @Override
    public void put(@NonNull final K key, final V value) {
        log.debug("Put key: {} with value: {} to cache", key, value);
        final Map<K, V> map = cache.getContent();

        synchronized (map) {
            if (map.containsKey(key)) {
                log.debug("Key {} already exist in cache. Previous key will be remove", key);
                map.remove(key);
            }

            if (cache.getSize().get() == map.size()) {
                log.debug("Cache size is exceeded. Least recent used item will be removed");
                removeLeastUsedItem(map);
            }

            map.put(key, value);
        }
    }

    @Override
    public void changeCapacity(@NonNull final Integer capacity) {
        log.debug("Resize cache to new capacity: {}. Current capacity: {}", capacity, cache.getSize().get());

        if (capacity < 0){
            throw new IllegalArgumentException("Capacity must be positive");
        }

        if (capacity == cache.getSize().get()) {
            log.debug("New and current capacity is equal");
            return;
        }

        if (capacity < cache.getSize().get()) {
            final int numberOfItemsToRemove = cache.getSize().get() - capacity;
            log.debug("New capacity is smaller then current. {} least recently used items will be removed", numberOfItemsToRemove);
            removeLeastUsedItems(cache.getContent(), numberOfItemsToRemove);
        }

        cache.getSize().set(capacity);
    }

    @Override
    public void invalidate() {
        final Map<K, V> map = cache.getContent();
        synchronized (map) {
            map.clear();
        }
    }

    //Remove only first item O(1)
    private void removeLeastUsedItem(final Map<K, V> map) {
        if (!map.isEmpty()) {
            map.remove(map.entrySet().iterator().next().getKey());
        }
    }

    //Remove n element worst case (if clearing all table) O(n)
    private void removeLeastUsedItems(final Map<K, V> map, final int numberItemsToRemove) {
        synchronized (map) {
            if (numberItemsToRemove >= map.size()) {
                map.clear();
                return;
            }

            final Iterator<Map.Entry<K, V>> iterator = map.entrySet().iterator();
            IntStream.rangeClosed(1, numberItemsToRemove).forEach(index -> {
                iterator.next();
                iterator.remove();
            });
        }
    }

    // remove amortized O(1), put O(1)
    public void moveAsynchronouslyToEndOfCache(final Map<K, V> map, final K key, final V value) {
        executor.submit(() -> {
            map.remove(key);
            map.put(key, value);
        });
    }
}
