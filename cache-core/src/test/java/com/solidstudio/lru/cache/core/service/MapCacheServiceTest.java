package com.solidstudio.lru.cache.core.service;

import com.solidstudio.lru.cache.core.domain.MapCache;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class MapCacheServiceTest {

    private static final int MAP_SIZE = 5;

    @Test
    public void shouldReturnProperElementAndMoveItToTheTail() {
        //given
        final Map<Object, Object> map = new LinkedHashMap<>();
        map.put(1, 1);
        map.put(2L, 2);
        map.put("123", 3);
        final MapCache cache = new MapCache(MAP_SIZE);
        cache.getContent().putAll(map);

        //when
        final Object result = new MapCacheService<>(cache).get(1);

        //then
        final Map<Object, Object> expected = new LinkedHashMap<>();
        expected.put(2L, 2);
        expected.put("123", 3);
        expected.put(1, 1);
        assertThat(result).isEqualTo(1);
        assertThat(cache.getContent()).isEqualTo(expected);
    }

    @Test
    public void shouldThrowExceptionDuringRetrievingWhenMapIsEmpty() {
        //given
        final MapCache cache = new MapCache(MAP_SIZE);

        //then
        assertThatThrownBy(() -> new MapCacheService<>(cache).get(1)).isInstanceOf(NoSuchElementException.class);
    }

    @Test
    public void shouldThrowExceptionDuringRetrievingWhenElementNotExist() {
        //given
        final Map<Object, Object> map = new LinkedHashMap<>();
        map.put(1, 1);
        map.put(2, 2);
        final MapCache cache = new MapCache(MAP_SIZE);
        cache.getContent().putAll(map);

        //then
        assertThatThrownBy(() -> new MapCacheService<>(cache).get(66)).isInstanceOf(NoSuchElementException.class);
    }

    @Test
    public void shouldReturnNullWhenValueIsNull() {
        //given
        final Map<Object, Object> map = new LinkedHashMap<>();
        map.put(1, null);
        final MapCache cache = new MapCache(MAP_SIZE);
        cache.getContent().putAll(map);

        //when
        final Object result = new MapCacheService<>(cache).get(1);

        //then
        assertThat(result).isNull();
    }

    @Test
    public void shouldPutNewElement() {
        //given
        final MapCache cache = new MapCache(MAP_SIZE);

        //when
        new MapCacheService<>(cache).put(1, "value");

        //then
        final Map actual = cache.getContent();
        assertThat(actual.size()).isEqualTo(1);
        assertThat(actual.get(1)).isEqualTo("value");
    }

    @Test
    public void shouldPutNewElementWithNullValue() {
        //given
        final MapCache cache = new MapCache(MAP_SIZE);

        //when
        new MapCacheService<>(cache).put(1, null);

        //then
        final Map actual = cache.getContent();
        assertThat(actual.size()).isEqualTo(1);
        assertThat(actual.get(1)).isNull();
    }

    @Test
    public void shouldPutNewElementWithExistingKeyAndMoveItToTheTail() {
        //given
        final Map<Object, Object> map = new LinkedHashMap();
        map.put(1, 1);
        map.put(2, 2);
        map.put("123", 3);
        map.put(12.0, 4);
        map.put("123.66", 1);
        final MapCache cache = new MapCache(MAP_SIZE);
        cache.getContent().putAll(map);

        //when
        new MapCacheService<>(cache).put(2, "testValue");

        //then
        final Map<Object, Object> expected = new LinkedHashMap();
        expected.put(1, 1);
        expected.put("123", 3);
        expected.put(12.0, 4);
        expected.put("123.66", 1);
        expected.put(2, "testValue");
        assertThat(cache.getContent()).isEqualTo(expected);
    }

    @Test
    public void shouldPutNewElementAndRemoveLeastRecentUsed() {
        //given
        final Map<Object, Object> map = new LinkedHashMap<>();
        map.put(1, 1);
        map.put(2, 2);
        map.put("123", 3);
        map.put(12.0, 4);
        map.put("123.11", 1);
        final MapCache cache = new MapCache(MAP_SIZE);
        cache.getContent().putAll(map);

        //when
        new MapCacheService<>(cache).put(13, "testValue");

        //then
        final Map<Object, Object> expected = new LinkedHashMap<>();
        expected.put(2, 2);
        expected.put("123", 3);
        expected.put(12.0, 4);
        expected.put("123.11", 1);
        expected.put(13, "testValue");
        assertThat(cache.getContent()).isEqualTo(expected);
    }

    @Test
    public void shouldClearAllIfSizeIs0() {
        //given
        final Map<Object, Object> map = new LinkedHashMap<>();
        map.put(1, 1);
        map.put(2, 2);
        map.put("123", 3);
        map.put(12.0, 4);
        map.put("123.11", 1);
        final MapCache cache = new MapCache(MAP_SIZE);
        cache.getContent().putAll(map);

        //when
        new MapCacheService<>(cache).changeCapacity(0);

        //then
        assertThat(cache.getContent()).isEmpty();
        assertThat(cache.getSize().get()).isEqualTo(0);
    }

    @Test
    public void shouldClearLEastRecentUsedIfCapacityIsLess() {
        //given
        final Map<Object, Object> map = new LinkedHashMap<>();
        map.put(1, 1);
        map.put(2, 2);
        map.put("123", 3);
        map.put(12.0, 4);
        map.put("123.11", 1);
        final MapCache cache = new MapCache(MAP_SIZE);
        cache.getContent().putAll(map);

        //when
        new MapCacheService<>(cache).changeCapacity(3);

        //then
        final Map<Object, Object> expected = new LinkedHashMap<>();
        expected.put("123", 3);
        expected.put(12.0, 4);
        expected.put("123.11", 1);
        assertThat(cache.getContent()).isEqualTo(expected);
        assertThat(cache.getSize().get()).isEqualTo(3);
    }

    @Test
    public void shouldResize() {
        //given
        final Map<Object, Object> map = new LinkedHashMap<>();
        map.put(1, 1);
        map.put(2, 2);
        map.put("123", 3);
        map.put(12.0, 4);
        map.put("123.11", 1);
        final MapCache cache = new MapCache(MAP_SIZE);
        cache.getContent().putAll(map);

        //when
        new MapCacheService<>(cache).changeCapacity(10);

        //then
        final Map<Object, Object> expected = new LinkedHashMap<>();
        expected.put(1, 1);
        expected.put(2, 2);
        expected.put("123", 3);
        expected.put(12.0, 4);
        expected.put("123.11", 1);
        assertThat(cache.getContent()).isEqualTo(expected);
        assertThat(cache.getSize().get()).isEqualTo(10);
    }

    @Test
    public void shouldInvaldiateCache() {
        //given
        final Map<Object, Object> map = new LinkedHashMap<>();
        map.put(1, 1);
        map.put(2, 2);
        map.put("123", 3);
        map.put(12.0, 4);
        map.put("123.11", 1);
        final MapCache cache = new MapCache(MAP_SIZE);
        cache.getContent().putAll(map);

        //when
        new MapCacheService<>(cache).invalidate();

        //then
        assertThat(cache.getContent()).isEmpty();
        assertThat(cache.getSize().get()).isEqualTo(MAP_SIZE);
    }
}