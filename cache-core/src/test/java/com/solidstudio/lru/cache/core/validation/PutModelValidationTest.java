package com.solidstudio.lru.cache.core.validation;


import com.solidstudio.lru.cache.core.dto.PutModel;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class PutModelValidationTest {
    private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @Test
    public void shouldValidateModel() {
        //given
        final PutModel<Integer, Integer> model = model(1, 1);

        //when
        final Set<ConstraintViolation<PutModel>> constraintViolations = validator.validate(model);

        //then
        assertThat(constraintViolations).hasSize(0);
    }

    @Test
    public void shouldNotValidateModel() {
        //given
        final PutModel<Integer, Integer> model = model(null, 1);

        //when
        final Set<ConstraintViolation<PutModel>> constraintViolations = validator.validate(model);

        //then
        assertThat(constraintViolations).hasSize(1);
    }

    private PutModel<Integer, Integer> model(final Integer key, final Integer value) {
        final PutModel<Integer, Integer> model = new PutModel<>();
        model.setKey(key);
        model.setValue(value);

        return model;
    }
}