package com.solistudio.lru.cache.web.config;

import com.google.common.base.Predicate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.base.Predicates.not;
import static com.google.common.base.Predicates.or;
import static com.google.common.collect.Lists.newArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    private static final String API_NAME = "LRU CACHE";

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).useDefaultResponseMessages(false).apiInfo(apiInfo()).select().paths
                (mergedInclusionsAndExclusions()).build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title(API_NAME).build();
    }

    private Predicate<String> mergedInclusionsAndExclusions() {
        return or(not(includeAllFromList(newArrayList("/system.*", "/swagger", "/error"))));
    }

    private Predicate includeAllFromList(final List<String> predicates) {
        return or(arrayOfPredicates(predicates));
    }

    private Predicate[] arrayOfPredicates(final List<String> paths) {
        return paths.stream().map(PathSelectors::regex).collect(Collectors.toList())
                .toArray(new Predicate[paths.size()]);
    }
}
