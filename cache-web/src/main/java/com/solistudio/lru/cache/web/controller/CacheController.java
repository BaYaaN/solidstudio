package com.solistudio.lru.cache.web.controller;

import com.solidstudio.lru.cache.core.dto.PutModel;
import com.solidstudio.lru.cache.core.service.CacheFacade;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cache")
public class CacheController {

    private final CacheFacade<Integer, Integer> cacheFacade;

    public CacheController(final CacheFacade<Integer, Integer> cacheFacade) {
        this.cacheFacade = cacheFacade;
    }

    @ApiOperation(value = "Get value from cache")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Value successful retrieved"),
            @ApiResponse(code = 404, message = "Key does not exist"),
            @ApiResponse(code = 500, message = "Internal server error")})
    @GetMapping("/key/{key}")
    private Integer get(@PathVariable("key") final Integer key) {
        return cacheFacade.get(key);
    }

    @ApiOperation(value = "Add new item to cached")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Item successful added"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Internal server error")})
    @PutMapping
    private void put(@RequestBody @Valid final PutModel<Integer, Integer> model) {
        cacheFacade.put(model.getKey(), model.getValue());
    }

    @ApiOperation(value = "Resize cache")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Cache successful resized"),
            @ApiResponse(code = 500, message = "Internal server error")})
    @PostMapping
    private void changeCapacity(@RequestParam("capacity") final Integer capacity) {
        cacheFacade.changeCapacity(capacity);
    }

    @ApiOperation(value = "Invalidate cache")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Cache successful invalidated"),
            @ApiResponse(code = 500, message = "Internal server error")})
    @DeleteMapping
    private void invalidate() {
        cacheFacade.invalidate();
    }
}
