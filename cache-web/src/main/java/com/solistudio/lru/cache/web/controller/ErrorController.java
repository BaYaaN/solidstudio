package com.solistudio.lru.cache.web.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.NoSuchElementException;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@Slf4j
@ControllerAdvice(annotations = RestController.class)
public class ErrorController {

    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public ResponseEntity<String> handleValidationException(final RuntimeException exception) {
        log.error(exception.toString(), exception);

        return new ResponseEntity<>("Upss something went wrong !", INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(NoSuchElementException.class)
    @ResponseBody
    public ResponseEntity<Integer> handleNoSuchElementException(final NoSuchElementException exception) {
        log.warn(exception.toString(), exception);

        return new ResponseEntity<>(-1, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseBody
    public ResponseEntity<String> handlefinal (final IllegalArgumentException exception) {
        log.error(exception.toString(), exception);

        return new ResponseEntity<>("Some arguments are incorrect", INTERNAL_SERVER_ERROR);
    }
}