package com.solistudio.lru.cache.web.integration;

import com.solidstudio.lru.cache.core.dto.PutModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.MalformedURLException;
import java.net.URL;

import static com.jayway.restassured.RestAssured.given;
import static javax.servlet.http.HttpServletResponse.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("mapCache")
public class FlowIntegrationTest {

    private static final String PROTOCOL = "http";
    private static final String HOST = "localhost";
    private static final String CACHE_URL = "/cache";
    private static final String GET_CACHE_URL = "/cache/key/{key}";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String CONTENT_TYPE_VALUE = "application/json";
    private static final String KEY_PARAM = "key";
    private static final String CAPACITY_PARAM = "capacity";
    private static final Integer KEY_NOT_EXIST = -1;

    @LocalServerPort
    private int port;

    @Before
    public void setUp() {
        invalidateCache();
        changeCapacity(2,SC_OK);
    }

    @Test
    public void shouldReturn404andIfKeyDoesNotExist() {
        get(1, KEY_NOT_EXIST, SC_NOT_FOUND);
    }

    @Test
    public void shouldAddNewItem() {
        //when
        put(model(1, 12));

        //then
        get(1, 12, SC_OK);
    }

    @Test
    public void shouldInvalidateCache() {
        //given
        put(model(1, 12));

        //when
        invalidateCache();

        //then
        get(1, KEY_NOT_EXIST, SC_NOT_FOUND);
    }

    @Test
    public void shouldAddElementAfterIncreaseCapacityOfCache() {
        //given
        put(model(1, 11));
        put(model(2, 12));

        //when
        changeCapacity(3, SC_OK);
        put(model(3, 12));

        //then
        get(3, 13, SC_OK);
    }

    @Test
    public void shouldRemoveLeastUsedItemWhenResizeCache() {
        //given
        put(model(1, 11));
        put(model(2, 12));

        //when
        changeCapacity(1,SC_OK);

        //then
        get(1, KEY_NOT_EXIST, SC_NOT_FOUND);
        get(2, 12, SC_OK);
    }

    @Test
    public void shouldRemoveLeastUsedItem() {
        //when
        put(model(1, 11));
        put(model(2, 12));

        //then
        get(1, 11, SC_OK);
        changeCapacity(1, SC_OK);

        //when
        get(1, 11, SC_OK);
        get(2, 12, SC_NOT_FOUND);
    }

    @Test
    public void shouldThrow500WhenCapacityIsNotPositive() {
        changeCapacity(-1, SC_INTERNAL_SERVER_ERROR);
    }

    private void invalidateCache(){
        given()
            .log()
            .all()
        .when()
            .delete(generateUrl(CACHE_URL))
        .then()
            .statusCode(SC_OK);
    }

    private void put (final PutModel<Integer, Integer> model){
        given()
            .log()
            .all()
            .header(CONTENT_TYPE, CONTENT_TYPE_VALUE)
            .body(model)
        .when()
            .put(generateUrl(CACHE_URL))
        .then()
            .statusCode(SC_OK);
    }

    private void get (final Integer key, final Integer value, final Integer httpStatus){
        given()
            .log()
            .all()
            .pathParam(KEY_PARAM, key)
        .when()
            .get(generateUrl(GET_CACHE_URL))
        .then()
            .statusCode(httpStatus)
            .equals(value);
    }

    private void changeCapacity (final Integer capacity, final Integer httpStatus){
        given()
            .log()
            .all()
            .param(CAPACITY_PARAM, capacity)
        .when()
            .post(generateUrl(CACHE_URL))
        .then()
            .statusCode(httpStatus);
    }

    private URL generateUrl(final String path) {
        try {
            return new URL(PROTOCOL, HOST, port, path);
        } catch (MalformedURLException e) {
            throw new RuntimeException("Can not generate proper URL");
        }
    }

    private PutModel<Integer, Integer> model(final Integer key, final Integer value) {
        final PutModel<Integer, Integer> model = new PutModel<>();
        model.setKey(key);
        model.setValue(value);

        return model;
    }
}
